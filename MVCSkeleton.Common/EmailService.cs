﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MVCSkeleton.Common
{
    public class EmailService
    {
        private readonly string _defaultEmailAddress;
        public EmailService(string defaultEmailAddress)
        {
            this._defaultEmailAddress = defaultEmailAddress;
        }

        public Task SendMail(IEnumerable<string> toAddress, string subject, string content, bool isHtml = true,
            string fromAddress = null)
        {

            var from = new MailAddress(string.IsNullOrEmpty(fromAddress) ? _defaultEmailAddress : fromAddress);

            var mailMessage = new MailMessage()
            {
                From = from,
                Subject = subject,
                Body = content,
                IsBodyHtml = isHtml
            };

            foreach (var address in toAddress)
            {
                mailMessage.To.Add(new MailAddress(address));
            }

            var smtp = new SmtpClient();
            smtp.Send(mailMessage);

            return Task.FromResult(0);
        }
    }
}
