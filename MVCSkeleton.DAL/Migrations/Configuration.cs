using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MVCSkeleton.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MVCSkeleton.DAL.DatabaseContext context)
        {
            var userManager = new UserManager<User, string>(new UserStore<User>(context));
            var roleManager = new RoleManager<IdentityRole, string>(new RoleStore<IdentityRole>(context));

            var adminRole = roleManager.FindByName("Admin");
            if (adminRole == null)
            {
                roleManager.Create(new IdentityRole("Admin"));
            }

            var userRole = roleManager.FindByName("User");
            if (userRole == null)
            {
                roleManager.Create(new IdentityRole("User"));
            }

            //Create admin user
            var adminUser = userManager.FindByName("Admin");
            if (adminUser == null)
            {
                userManager.Create(new User()
                {
                    UserName = "Admin",
                    Email = "admin@admin.lk",
                    UserConfirmed = true,
                    EmailConfirmed = true,
                }, "password");

                var adminUserId = userManager.FindByName("Admin").Id;
                userManager.AddToRole(adminUserId, "Admin");

            }

        }
    }
}
