﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace MVCSkeleton.DAL
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        public DatabaseContext(string connectionString) 
            : base(connectionString)
        {
        }

        public DatabaseContext() : base("DefaultConnection")
        {
        }

    }



}
