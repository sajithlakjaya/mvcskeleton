﻿namespace MVCSkeleton.Web.Models
{
    public class StatWidgetModel
    {
        public string Header { get; set; }
        public string SubHeader { get; set; }
        public string Icon { get; set; }
        public string Data { get; set; }
        public string Color { get; set; }
    }
}
