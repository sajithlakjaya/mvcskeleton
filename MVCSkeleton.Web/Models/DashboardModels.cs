﻿namespace MVCSkeleton.Web.Models
{
    public class StartWidgetModel
    {
        public string Icon { get; set; }
        public string MainText { get; set; }
        public string MutedText{ get; set; }
        public decimal Progress { get; set; }

    }
}
