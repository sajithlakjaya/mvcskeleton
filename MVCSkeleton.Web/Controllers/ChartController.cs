﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace MVCSkeleton.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class ChartController : Controller
    {

        public ActionResult Charts()
        {
            return View();
        }

        public ActionResult Float()
        {
            return View();
        }

        public ActionResult BarChart()
        {
            var chart = new Chart(width: 300, height: 200)
              .AddSeries(
                          chartType: "column",
                          xValue: new[] { "10 Records", "20 Records", "30 Records", "40 Records" },
                          yValues: new[] { "50", "60", "78", "80" })
                          .GetBytes("png");
            return File(chart, "image/bytes");
        }

        public ActionResult PieChart()
        {
            var chart = new Chart(width: 300, height: 200)
              .AddSeries(
                          chartType: "pie",
                          xValue: new[] { "10 Records", "20 Records", "30 Records", "40 Records" },
                          yValues: new[] { "50", "60", "78", "80" })
                          .GetBytes("png");
            return File(chart, "image/bytes");
        }

        public ActionResult StackedBarChart()
        {
            var chart = new Chart(width: 300, height: 200)
              .AddSeries(
                          chartType: "bar",
                          xValue: new[] { "10 Records", "20 Records", "30 Records", "40 Records" },
                          yValues: new[] { "50", "60", "78", "80" })
                          .GetBytes("png");
            return File(chart, "image/bytes");
        }

        public ActionResult LineChart()
        {
            var chart = new Chart(width: 300, height: 200)
              .AddSeries(
                          chartType: "line",
                          xValue: new[] { "10 Records", "20 Records", "30 Records", "40 Records" },
                          yValues: new[] { "50", "60", "78", "80" })
                          .GetBytes("png");
            return File(chart, "image/bytes");
        }

        public ActionResult Radial()
        {
            return View();
        }

    }
}