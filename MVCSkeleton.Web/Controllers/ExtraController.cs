﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSkeleton.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class ExtraController : Controller
    {
        public ActionResult Calender()
        {
            return View();
        }

        public ActionResult Timeline()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult Invoice()
        {
            return View();
        }

        public ActionResult Mailbox()
        {
            return View();
        }

        public ActionResult Filebox()
        {
            return View();
        }
    }
}