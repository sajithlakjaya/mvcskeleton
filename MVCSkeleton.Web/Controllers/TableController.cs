﻿using System.Web.Mvc;

namespace MVCSkeleton.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class TableController : Controller
    {
        public ActionResult DataTable()
        {
            return View();
        }

        public ActionResult Standard()
        {
            return View();
        }

        public ActionResult Extended()
        {
            return View();
        }

    }
}