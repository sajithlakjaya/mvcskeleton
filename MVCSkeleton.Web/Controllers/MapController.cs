﻿using System.Web.Mvc;

namespace MVCSkeleton.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class MapController : Controller
    {
        public ActionResult Google()
        {
            return View();
        }

        public ActionResult Vector()
        {
            return View();
        }
    }
}