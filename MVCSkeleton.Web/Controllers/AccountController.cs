﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using MVCSkeleton.BLL.BllException;
using MVCSkeleton.BLL.Security;
using MVCSkeleton.Common;
using MVCSkeleton.DAL;
using MVCSkeleton.Web.Models;
using reCAPTCHA.MVC;

namespace MVCSkeleton.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly SecurityService _securityService;
        private readonly EmailService _emailService;

        public AccountController(SecurityService securityService, EmailService emailService)
        {
            _securityService = securityService;
            _emailService = emailService;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _securityService.SignInAsync(model.Email, model.Password, model.RememberMe, model.RememberMe);
                    var currentUser = await _securityService.GetCurrentUser();
                    if (currentUser.UserName == "Admin")
                    {
                        return RedirectToAction("Index", "Users", new {Area = "Admin"});
                    }
                    if (!currentUser.UserConfirmed)
                    {
                        return View("NotApprovalUser");
                    }
                    return RedirectToAction("Dashboard", "Home");
                }
                catch (ServiceException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }
                return View(model);
            }

            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [CaptchaValidator]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _securityService.CreateUser(new User()
                    {
                        Email = model.Email,
                        UserName = model.Email
                    }, model.Password);

                    //Add user role into the system
                    await _securityService.AddRoleToUser(model.Email, "User");
                    var code = System.Web.HttpUtility.UrlEncode(result.Token);
                    await _emailService.SendMail(new[] { model.Email }, "Email Confirmation",
                        $@"Please click <a href='http://localhost:64194/Account/ConfirmEmail?Token={code}&UserId={result.UserId}'>here</a> to confirm email address");
                    return RedirectToAction("Login");
                }
                catch (ServiceException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //GET : /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(ConfirmEmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                               .SelectMany(v => v.Errors)
                               .Select(e => e.ErrorMessage));
                return View("EmailConfirmationError", new EmailConfirmationErrorModel()
                {
                    Message = message
                });
            }
            try
            {
                await _securityService.ConfirmEmail(model.UserId, model.Token);
            }
            catch (ServiceException e)
            {
                return View("EmailConfirmationError", new EmailConfirmationErrorModel()
                {
                    Message = e.Message
                });
            }

            return View("EmailConfirmationSuccess");
        }

        //
        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            _securityService.SignOut();
            return RedirectToAction("Login", "Account");
        }

        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        #endregion
    }
}