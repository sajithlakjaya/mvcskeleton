﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSkeleton.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    public class FormController : Controller
    {
        public ActionResult Standard()
        {
            return View();
        }

        public ActionResult Wizard()
        {
            return View();
        }

        public ActionResult Validation()
        {
            return View();
        }

        public ActionResult Extended()
        {
            return View();
        }
    }
}