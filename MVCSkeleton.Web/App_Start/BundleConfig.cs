﻿using System.Web.Optimization;

namespace MVCSkeleton.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            //Plugins
            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                "~/assets/vendor/chosen/chosen.jquery.min.js",
                "~/assets/vendor/slider/js/bootstrap-slider.js",
                "~/assets/vendor/filestyle/bootstrap-filestyle.min.js"));

            //Animo
            bundles.Add(new ScriptBundle("~/bundles/animo").Include(
                "~/assets/vendor/animo/animo.min.js"));

            //Slimscroll
            bundles.Add(new ScriptBundle("~/bundles/Slimscroll").Include(
                "~/assets/vendor/slimscroll/jquery.slimscroll.min.js"));

            //Sparklines
            bundles.Add(new ScriptBundle("~/bundles/Sparklines").Include(
                "~/assets/vendor/sparklines/jquery.sparkline.min.js"));

            //Store + JSON
            bundles.Add(new ScriptBundle("~/bundles/StoreJson").Include(
                "~/assets/vendor/store/store+json2.min.js"));

            //Classyloader
            bundles.Add(new ScriptBundle("~/bundles/Classyloader").Include(
                "~/assets/vendor/classyloader/js/jquery.classyloader.min.js"));

            //Flot Charts
            bundles.Add(new ScriptBundle("~/bundles/Flowchart").Include(
               "~/assets/vendor/flot/jquery.flot.min.js",
               "~/assets/vendor/flot/jquery.flot.tooltip.min.js",
               "~/assets/vendor/flot/jquery.flot.resize.min.js",
               "~/assets/vendor/flot/jquery.flot.pie.min.js",
               "~/assets/vendor/flot/jquery.flot.time.min.js",
               "~/assets/vendor/flot/jquery.flot.categories.min.js"));

            //App
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/assets/app/js/app.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/assets/app/css/bootstrap.css",
                      "~/assets/vendor/fontawesome/css/font-awesome.min.css",
                      "~/assets/vendor/animo/animate+animo.css",
                      "~/assets/vendor/csspinner/csspinner.min.css",
                      "~/assets/app/css/app.css"));

            bundles.Add(new StyleBundle("~/Content/commoncss").Include(
                "~/assets/app/css/common.css"));

            bundles.Add(new StyleBundle("~/Content/landingcss").Include(
                "~/assets/app/css/landing.css"));

            bundles.Add(new ScriptBundle("~/bundles/pagescripts").Include(
                "~/assets/vendor/animo/animo.min.js",
                "~/assets/app/js/pages.js"));

            bundles.Add(new ScriptBundle("~/bundles/mapscripts").Include(
                "~/assets/vendor/gmap/jquery.gmap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/classyloader").Include(
                "~/assets/vendor/classyloader/js/jquery.classyloader.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/vectormap").Include(
                "~/assets/vendor/jvectormap/jquery-jvectormap-1.2.2.min.js",
                "~/assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/assets/vendor/jqueryui/js/jquery-ui-1.10.4.custom.min.js",
                "~/assets/vendor/touch-punch/jquery.ui.touch-punch.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                "~/assets/vendor/moment/min/moment-with-langs.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/fullcalender").Include(
                "~/assets/vendor/fullcalendar/fullcalendar.min.js"));

            bundles.Add(new StyleBundle("~/Content/jqueryui").Include(
                "~/assets/vendor/jqueryui/css/ui-lightness/jquery-ui-1.10.4.custom.min.css"));

            bundles.Add(new StyleBundle("~/Content/fullcalender").Include(
                "~/assets/vendor/fullcalendar/fullcalendar.css",
                "~/assets/vendor/fullcalendar/fullcalendar.print.css"));

            bundles.Add(new StyleBundle("~/Content/codemirror").Include(
                "~/assets/vendor/codemirror/lib/codemirror.css"));

            bundles.Add(new ScriptBundle("~/Content/codemirrow").Include(
                "~/assets/vendor/codemirror/lib/codemirror.js",
                "~/assets/vendor/codemirror/addon/mode/overlay.js",
                "~/assets/vendor/codemirror/mode/markdown/markdown.js",
                "~/assets/vendor/codemirror/mode/xml/xml.js",
                "~/assets/vendor/codemirror/mode/gfm/gfm.js",
                "~/assets/vendor/marked/marked.js"));

            bundles.Add(new StyleBundle("~/Content/boostraptag").Include(
                "~/assets/vendor/tagsinput/bootstrap-tagsinput.css"));

            bundles.Add(new ScriptBundle("~/bundles/bwizard").Include(
                "~/assets/vendor/wizard/js/bwizard.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/parseley").Include(
                "~/assets/vendor/parsley/parsley.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                "~/assets/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"));

        }
    }
}
