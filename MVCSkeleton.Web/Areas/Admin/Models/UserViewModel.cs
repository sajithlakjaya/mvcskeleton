﻿using System.Collections.Generic;
using MVCSkeleton.DAL;

namespace MVCSkeleton.Web.Areas.Admin.Models
{
    public class UserViewModel
    {
        public string Search{ get; set; }
        public bool EmailConfirmed { get; set; }
        public bool UserConfirmed { get; set; }

        public IEnumerable<User> Users { get; set; }
    }
}