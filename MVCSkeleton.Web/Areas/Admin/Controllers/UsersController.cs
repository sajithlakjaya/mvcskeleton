﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MVCSkeleton.BLL.Security;
using MVCSkeleton.Web.Areas.Admin.Models;

namespace MVCSkeleton.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly SecurityService _securityService;
        public UsersController(SecurityService securityService)
        {
            _securityService = securityService;
        }

        // GET: Admin/Users
        public ActionResult Index(UserViewModel model)
        {
            var users = _securityService.GetUsers().Where(c => c.UserName != "Admin");
            //Filter by seach string
            if (!string.IsNullOrEmpty(model.Search))
            {
                users = users.Where(c => c.UserName.Contains(model.Search));
            }

            //Filter by email confirmation
            users = users.Where(c => c.EmailConfirmed == model.EmailConfirmed);

            //Filter by userConfirmation
            users = users.Where(c => c.UserConfirmed == model.UserConfirmed);

            model.Users = users;

            return View(model);
        }

        public async Task<ActionResult> UserConfirm(string userId)
        {
            await _securityService.UserConfirm(userId);
            return RedirectToAction("Index");
        }

    }
}