﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using MVCSkeleton.BLL.BllException;
using MVCSkeleton.BLL.Security.Util;
using MVCSkeleton.DAL;

namespace MVCSkeleton.BLL.Security
{
    public class SecurityService
    {
        private DatabaseContext _context;
        private ApplicationUserManager userManager;
        private IAuthenticationManager authenticationManager;

        public SecurityService(DatabaseContext context, IAuthenticationManager authenticationManager)
        {
            _context = context;
            this.authenticationManager = authenticationManager;
            userManager = new ApplicationUserManager(new UserStore<User>((DbContext)context));
        }

        public async Task<CreateUserResponse> CreateUser(User user, string password)
        {
            var result = await userManager.CreateAsync(user, password);

            if (result.Succeeded == false)
                throw new ServiceException(string.Join(", ", result.Errors));

            var token = await userManager.GenerateEmailConfirmationTokenAsync(user.Id);

            return new CreateUserResponse()
            {
                Token = token,
                UserId = user.Id
            };
        }

        public async Task<bool> AddRoleToUser(string username, string rolename)
        {
            var user = await userManager.FindByNameAsync(username);
            var result = await userManager.AddToRoleAsync(user.Id, rolename);

            if (result.Succeeded == false)
                throw new ServiceException(string.Join(", ", result.Errors));

            return true;
        }

        public async Task<string> ForgotPassword(string email)
        {
            var user = await userManager.FindByEmailAsync(email);

            if (user == null)
                throw new ServiceException("User not founded");

            if (!await userManager.IsEmailConfirmedAsync(user.Id))
                throw new ServiceException("Email is not confirmed");

            var code = await userManager.GeneratePasswordResetTokenAsync(user.Id);

            return code;
        }

        public async Task SignInAsync(string username, string password, bool isPersistent, bool rememberBrowser)
        {
            // Clear any partial cookies from external or two factor partial sign ins
            authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie,
                DefaultAuthenticationTypes.TwoFactorCookie);

            var user = await userManager.FindByEmailAsync(username);

            if (user == null)
                throw new ServiceException("Unable to find a user for that email");

            if (!user.EmailConfirmed)
                throw new ServiceException("Email is not confirmed");

            var isValidPassword = await userManager.CheckPasswordAsync(user, password);

            if (!isValidPassword)
                throw new ServiceException("Invalid Password");

            var userIdentity = await user.GenerateUserIdentityAsync(userManager);

            if (rememberBrowser)
            {
                var rememberBrowserIdentity =
                    authenticationManager.CreateTwoFactorRememberBrowserIdentity(user.Id);

                authenticationManager.SignIn(
                    new AuthenticationProperties { IsPersistent = isPersistent },
                    userIdentity,
                    rememberBrowserIdentity);
            }
            else
            {
                authenticationManager.SignIn(
                    new AuthenticationProperties { IsPersistent = isPersistent },
                    userIdentity);
            }
        }

        public async Task<bool> ConfirmEmail(string userId, string token)
        {
            var user = await userManager.FindByIdAsync(userId);

            if (user == null)
                throw new ServiceException("Unable to find a user");

            try
            {

                if (user.EmailConfirmed)
                    throw new ServiceException("User already confirm the email");

                var result = await userManager.ConfirmEmailAsync(user.Id, token);

                if (result.Succeeded == false)
                    throw new ServiceException(string.Join(", ", result.Errors));

                return true;
            }
            catch
            {
                throw new ServiceException("Invalid token");
            }

        }

        public async Task<bool> ResetPassword(string username, string token, string newPassword)
        {
            var user = await userManager.FindByNameAsync(username);

            if (user == null)
                throw new ServiceException("Unable to find a user for that mail");

            var result = await userManager.ResetPasswordAsync(user.Id, token, newPassword);

            if (result.Succeeded == false)
                throw new ServiceException(string.Join(", ", result.Errors));

            return true;
        }

        public void SignOut()
        {
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public async Task<User> GetCurrentUser()
        {
            var userId = authenticationManager.User.Identity.GetUserId();
            var user = await userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ServiceException("Unable to find user");
            return user;
        }

        public async Task<bool> UpdateUser(UserUpdateRequest request)
        {

            var user = await userManager.FindByIdAsync(request.UserId);
            if (user == null)
                throw new ServiceException("Unable to find user");

            //Update the user informations

            var result = await userManager.UpdateAsync(user);
            if (result.Succeeded == false)
                throw new ServiceException(string.Join(", ", result.Errors));
            return true;
        }

        public async Task UserConfirm(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ServiceException("Unable to find user");

            user.UserConfirmed = true;

            var result = await userManager.UpdateAsync(user);
            if (result.Succeeded == false)
                throw new ServiceException(string.Join(", ", result.Errors));
        }

        public IEnumerable<User> GetUsers()
        {
            return userManager.Users;
        }

        public async Task<User> GetUser(UserLoginInfo userLoginInfo)
        {
            return await userManager.FindAsync(userLoginInfo);
        }

        public async Task SignInAync(User user)
        {
            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
                 OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = CreateProperties(user.UserName);
            authenticationManager.SignIn(properties, oAuthIdentity, cookieIdentity);
        }

        public void SignIn(ClaimsIdentity claimsIdentity)
        {
            authenticationManager.SignIn(claimsIdentity);
        }

        private static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }

    }
}
