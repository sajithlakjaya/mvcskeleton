﻿namespace MVCSkeleton.BLL.Security
{
    public class CreateUserResponse
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }

    public class UserUpdateRequest
    {
        public string UserId { get; set; }
    }
}
