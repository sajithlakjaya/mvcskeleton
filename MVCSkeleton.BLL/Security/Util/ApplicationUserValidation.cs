﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MVCSkeleton.DAL;

namespace MVCSkeleton.BLL.Security.Util
{
    public class ApplicationUserValidator : UserValidator<User>
    {
        private UserManager<User, string> _manager;

        public ApplicationUserValidator(UserManager<User, string> manager) : base(manager)
        {
            _manager = manager;
        }

        public override Task<IdentityResult> ValidateAsync(User item)
        {

            return base.ValidateAsync(item);
        }
    }

}
