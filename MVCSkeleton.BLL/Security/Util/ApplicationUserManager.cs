﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MVCSkeleton.DAL;

namespace MVCSkeleton.BLL.Security.Util
{
    public sealed class ApplicationUserManager : UserManager<User>
    {

        public ApplicationUserManager(IUserStore<User> store)
            : base(store)
        {
            Initial();
        }

        private void Initial()
        {
            // Configure validation logic for usernames
            UserValidator = new ApplicationUserValidator(this)
            {
                RequireUniqueEmail = true,
                AllowOnlyAlphanumericUserNames = false
            };

            // Configure validation logic for passwords
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            UserLockoutEnabledByDefault = true;
            DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<User>
            {
                MessageFormat = "Your security code is {0}"
            });
            RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<User>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });


            var provider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("mvc");
            UserTokenProvider =
                new DataProtectorTokenProvider<User>(provider.Create("EmailConfirmation"));
        }
    }
}
