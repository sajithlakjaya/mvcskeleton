﻿using System;

namespace MVCSkeleton.BLL.BllException
{
    public class ServiceException : Exception
    {
        /// <summary>
        /// Initial the service exception with message
        /// </summary>
        /// <param name="message">Exception message</param>
        public ServiceException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initial the service exception with message and inner exception
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception</param>
        public ServiceException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

        //Todo add other property for identity the service informations
    }
}
